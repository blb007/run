package com.example.blb.run.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.blb.run.model.User;

import static android.content.Context.MODE_PRIVATE;

public class LocalStore {
    private SharedPreferences userLocalDatabase;
    public LocalStore(Context context){
        userLocalDatabase = context.getSharedPreferences("User",0);
    }
    public void storeUserData(User user)
    {
        SharedPreferences.Editor spEditor =  userLocalDatabase.edit();
        spEditor.putString("name",user.getName());
        spEditor.putString("email",user.getEmail());
        spEditor.putInt("age",user.getAge());
        spEditor.putString("height",""+user.getHeight());
        spEditor.putString("weight",""+user.getWeight());
        spEditor.putString("phone",user.getPhone());
        spEditor.putString("uid",user.getUid());
        spEditor.putString("gender",user.getGender());
        spEditor.apply();
    }

    public User getUserData(){
            User user=new User();
            user.setName(userLocalDatabase.getString("name", ""));
            user.setEmail(userLocalDatabase.getString("email", ""));
            user.setAge(userLocalDatabase.getInt("age", 0));
            user.setHeight(Double.parseDouble(userLocalDatabase.getString("height", "0.0")));
            user.setWeight(Double.parseDouble(userLocalDatabase.getString("weight", "0.0")));
            user.setPhone(userLocalDatabase.getString("phone", ""));
            user.setUid(userLocalDatabase.getString("uid", ""));
            user.setGender(userLocalDatabase.getString("gender", ""));

            return user;
    }
    public void setUserLoggedIn(){
        SharedPreferences.Editor spEditor =  userLocalDatabase.edit();
        spEditor.putBoolean("loggedin",true);
        spEditor.apply();
    }
    public boolean isUserLoggedIn(){
        return userLocalDatabase.getBoolean("loggedin",false);
    }
    public void clearUserData()
    {
        SharedPreferences.Editor spEditor=userLocalDatabase.edit();
        spEditor.clear();
        spEditor.apply();
    }
}
