package com.example.blb.run.code;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.blb.run.R;
import com.example.blb.run.util.LocalStore;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignInActivity extends AppCompatActivity {
    EditText userName, password;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;
    LocalStore store;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userName=findViewById(R.id.editTextEmail);
        password=findViewById(R.id.editTextPassword);
        Button buttonSignIn = findViewById(R.id.buttonSignin);

        firebaseAuth = FirebaseAuth.getInstance();
        store=new LocalStore(getBaseContext());
        if(firebaseAuth.getCurrentUser() != null || store.isUserLoggedIn()){
            //close this activity
            finish();
            //opening profile activity
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        }
        TextView textViewSignup = findViewById(R.id.registerTv);
        TextView forgot = findViewById(R.id.forgot);
        progressDialog = new ProgressDialog(this);

        textViewSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getBaseContext(),SignUpActivity.class));
            }
        });
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignInActivity.this, ForgotActivity.class));
            }
        });

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = userName.getText().toString().trim();
                String pass  = password.getText().toString().trim();

                //checking if email and passwords are empty
                if(TextUtils.isEmpty(email)){
                    userName.setError("Please enter Email");
                }else if(TextUtils.isEmpty(pass)){
                    password.setError("Please enter password");
                }else{
                    login(email,pass);
                }
            }
        });
    }

    private void login(String email,String pass) {
        progressDialog.setMessage("Signing in Please Wait...");
        progressDialog.show();
        firebaseAuth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        //if the task is successfull
                        if(task.isSuccessful()){
                            //start the profile activity
                            store.setUserLoggedIn();
                            finish();
                            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        }
                        else
                        {
                            userName.setError("User name or password wrong");
                        }
                    }
                });
    }

}