package com.example.blb.run.code;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.blb.run.R;
import com.example.blb.run.model.User;
import com.example.blb.run.util.LocalStore;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity {
    //defining view objects
    private Button buttonSignup;
    private ProgressDialog progressDialog;
    private EditText age,phone,editTextEmail,editTextPassword,name;

    private TextView textViewSignin;
    //defining firebaseauth object
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //initializing firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() != null) {
            //that means user is already logged in
            //so close this activity
            finish();
            //and open profile activity
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        }

        //initializing views
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        textViewSignin = findViewById(R.id.textViewSignin);
        buttonSignup = findViewById(R.id.buttonSignup);
        name = findViewById(R.id.username);
        age = findViewById(R.id.userage);
        phone = findViewById(R.id.userphone);
        progressDialog = new ProgressDialog(this);

        //attaching listener to button
        buttonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });
        textViewSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getBaseContext(), SignInActivity.class));
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        /*
      ATTENTION: This was auto-generated to implement the App Indexing API.
      See https://g.co/AppIndexing/AndroidStudio for more information.
     */
        //  GoogleApiClient client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void registerUser() {

        //getting email and password from edit texts
        String uname = name.getText().toString().trim();
        String uage = age.getText().toString().trim();
        String uphone = phone.getText().toString().trim();
        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        // String re_pass = password.getText().toString().trim();

        //checking if email and passwords are empty
        if (TextUtils.isEmpty(uname)) {
            Toast.makeText(this, "Please enter Name", Toast.LENGTH_LONG).show();
            name.setError("Please enter Name");
            return;
        }
        if (TextUtils.isEmpty(uage)) {
            Toast.makeText(this, "Please enter Age", Toast.LENGTH_LONG).show();
            age.setError("Please enter Age");
            return;
        }
        if (TextUtils.isEmpty(uphone)) {
            Toast.makeText(this, "Please enter phone number", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Please enter email", Toast.LENGTH_LONG).show();
            editTextEmail.setError("Please enter email");
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_LONG).show();
            editTextPassword.setError("Please enter password");
            return;
        }
        if(password.length()<6){
            Toast.makeText(this, "Password length must be atleast 6", Toast.LENGTH_LONG).show();
            editTextPassword.setError("Password length must be atleast 6");
            return;
        }

        //if the email and password are not empty
        //displaying a progress dialog

        progressDialog.setMessage("Registering Please Wait...");
        progressDialog.show();

        //creating a new user
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if (task.isSuccessful()) {
                            //display some message here
                            Toast.makeText(getBaseContext(), "Successfully registered", Toast.LENGTH_LONG).show();
                            firebaseAuth.signInWithEmailAndPassword(email, password);
                            savedata();
                        } else {
                            //display some message here
                            Toast.makeText(getBaseContext(), "Registration Error", Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }
                });
        //call function to save user data

    }
    private void savedata(){
        DatabaseReference databaseReference;
        //getting the database reference
        databaseReference = FirebaseDatabase.getInstance().getReference();

        FirebaseUser user = firebaseAuth.getCurrentUser();

        String name1 = name.getText().toString().trim();
        //geting radio button value
        RadioGroup rGroup = findViewById(R.id.radioSex);
        RadioButton radioSexButton;
        // get selected radio button from radioGroup
        int selectedId = rGroup.getCheckedRadioButtonId();
        // find the radiobutton by returned id
        radioSexButton = findViewById(selectedId);

        Integer age1= Integer.valueOf(age.getText().toString().trim());
        String gender1 = radioSexButton.getText().toString().trim();
        String phone1 = phone.getText().toString().trim();

        //creating a user information object
        User userInformation = new User(name1,age1,gender1, phone1);
        databaseReference.child(user.getUid()).setValue(userInformation);

        LocalStore store=new LocalStore(getBaseContext());
        userInformation.setUid(user.getUid());
        userInformation.setEmail(editTextEmail.getText().toString().trim());
        userInformation.setWeight(0.0);
        userInformation.setHeight(0.0);
        store.storeUserData(userInformation);

        //Toast.makeText(this, "Information Saved...", Toast.LENGTH_LONG).show();
        finish();
        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
    }

}
